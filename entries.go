package annex

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/filemode"
	"github.com/go-git/go-git/v5/plumbing/format/index"
	"github.com/go-git/go-git/v5/plumbing/object"
)

type Entry struct {
	Name, Target string
	GitMode      filemode.FileMode
	GitHash      plumbing.Hash
	Key          *Key
	ModifiedAt   time.Time
}

func (e Entry) IsDir() bool { return e.GitMode == filemode.Dir }

type Entries struct {
	Repository *git.Repository
	headTree   *object.Tree
	index      *index.Index
	root       struct {
		sync.Once
		dir string
	}
}

func (es *Entries) symlinkTarget(te *object.TreeEntry) (string, error) {
	if te.Mode != filemode.Symlink {
		return "", fmt.Errorf("symlinkTarget: %v not a symlink", te)
	}
	blob, err := es.Repository.BlobObject(te.Hash)
	if err != nil {
		return "", fmt.Errorf("symlinkTarget(%s): Blob: %w", te.Hash, err)
	}
	bRdr, err := blob.Reader()
	if err != nil {
		return "", fmt.Errorf("symlinkTarget(%s): blob.Reader: %w", te.Hash, err)
	}
	defer bRdr.Close()
	bs, err := io.ReadAll(bRdr)
	if err != nil {
		return "", fmt.Errorf("symlinkTarget(%s): ReadAll: %w", te.Hash, err)
	}
	return string(bs), nil
}

func (es *Entries) statDirTimes(entry *Entry) {
	es.root.Do(func() {
		if wt, err := es.Repository.Worktree(); err == nil {
			es.root.dir = wt.Filesystem.Root()
		} else {
			log.Printf("startDirTimes: can't find worktree, so directories won't have ModifiedAt values: %v", err)
		}
	})
	if es.root.dir == "" {
		return
	}
	fi, err := os.Lstat(filepath.Join(es.root.dir, entry.Name))
	if err != nil {
		return
	}
	entry.ModifiedAt = fi.ModTime()
}

func (es *Entries) treeEntryToEntry(name string, te *object.TreeEntry) (*Entry, error) {
	entry := Entry{Name: name, GitMode: te.Mode, GitHash: te.Hash}
	if te.Mode == filemode.Symlink {
		target, err := es.symlinkTarget(te)
		if err != nil {
			return nil, err
		}
		entry.Target = target
		if strings.Contains(target, ".git/annex/objects") {
			key, err := NewKey(filepath.Base(target))
			if err != nil {
				return nil, fmt.Errorf("treeEntryToEntry(name: %s): NewKey: failed for %s: %w", name, target, err)
			}
			entry.Key = key
		}
	}
	if es.index == nil {
		idx, err := es.Repository.Storer.Index()
		if err != nil {
			return nil, err
		}
		es.index = idx
	}
	idxEntry, err := es.index.Entry(entry.Name)
	if err != nil {
		if te.Mode == filemode.Dir {
			es.statDirTimes(&entry)
		}
		return &entry, nil
	}
	entry.ModifiedAt = idxEntry.ModifiedAt
	return &entry, nil
}

func (es *Entries) setHeadTree() error {
	ref, err := es.Repository.Head()
	if err != nil {
		return err
	}
	headCommit, err := es.Repository.CommitObject(ref.Hash())
	if err != nil {
		return err
	}
	headTreeHash := headCommit.TreeHash
	tree, err := es.Repository.TreeObject(headTreeHash)
	if err != nil {
		return err
	}
	es.headTree = tree
	return nil
}

func (es *Entries) Dir(path string) ([]*Entry, error) {
	// todo: handle path being a symlink
	if es.headTree == nil {
		if err := es.setHeadTree(); err != nil {
			return nil, err
		}
	}
	dirTree, err := es.headTree.Tree(path)
	if err != nil {
		return nil, err
	}
	entries := []*Entry{}
	for _, te := range dirTree.Entries {
		entry, err := es.treeEntryToEntry(filepath.Join(path, te.Name), &te)
		if err != nil {
			return nil, err
		}
		entries = append(entries, entry)
	}
	return entries, nil
}

func (es *Entries) Entry(path string) (*Entry, error) {
	// todo: handle path being a symlink
	if es.headTree == nil {
		if err := es.setHeadTree(); err != nil {
			return nil, err
		}
	}
	path = strings.TrimPrefix(path, "/")
	te, err := es.headTree.FindEntry(path)
	if err != nil {
		return nil, err
	}
	return es.treeEntryToEntry(path, te)
}

// todo: does it make sense to handle non-symlinks like this?
func (es *Entries) EvalSymlinks(entry *Entry) (*Entry, error) {
	if entry.GitMode != filemode.Symlink || entry.Key != nil {
		return entry, nil
	}
	targetEntry, err := es.Entry(filepath.Join(filepath.Dir(entry.Name), entry.Target))
	if err != nil {
		return nil, err
	}
	if targetEntry.GitMode == filemode.Symlink {
		return es.EvalSymlinks(targetEntry)
	}
	return targetEntry, nil
}

func (es *Entries) Each(fn func(*Entry) error) error {
	if es.headTree == nil {
		if err := es.setHeadTree(); err != nil {
			return err
		}
	}
	seen := map[plumbing.Hash]bool{}
	tw := object.NewTreeWalker(es.headTree, true, seen)
	defer tw.Close()
	for {
		name, te, err := tw.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		entry, err := es.treeEntryToEntry(name, &te)
		if err != nil {
			return err
		}
		if err := fn(entry); err != nil {
			return err
		}
	}
	return nil
}
