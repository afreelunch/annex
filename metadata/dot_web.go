package metadata

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

type webLine struct {
	timeStamp, uri string
	present        bool
}
type dotWeb struct {
	Lines []*webLine
}

func newDotWeb() *dotWeb {
	return &dotWeb{Lines: []*webLine{}}
}

func (dw *dotWeb) Parse(rdr io.Reader) (*[]string, error) {
	if err := dw.parseLines(rdr); err != nil {
		return nil, err
	}
	return dw.parseFields(), nil
}

func (dw *dotWeb) parseLines(rdr io.Reader) error {
	dw.Lines = dw.Lines[:0] // fixme: mem leak
	br := bufio.NewReader(rdr)
	for {
		str, err := br.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		fields := strings.Split(str, " ")
		if len(fields) != 3 {
			return fmt.Errorf("invalid line: %s", str)
		}

		fields[2] = strings.TrimSuffix(strings.TrimPrefix(fields[2], ":"), "\n")
		wl := webLine{timeStamp: fields[0], uri: fields[2]}
		switch fields[1] {
		case "1":
			wl.present = true
		case "0":
			wl.present = false
		default:
			return fmt.Errorf("malformed line: %s", str)
		}
		dw.Lines = append(dw.Lines, &wl)
	}
	return nil
}

func (dw *dotWeb) parseFields() *[]string {
	urlsSeen := map[string]string{}
	urls := []string{}
	for _, ln := range dw.Lines {
		if ln.present {
			urlsSeen[ln.uri] = ln.timeStamp
		} else {
			delete(urlsSeen, ln.uri)
		}
	}
	for uri := range urlsSeen {
		urls = append(urls, uri)
	}
	return &urls
}
