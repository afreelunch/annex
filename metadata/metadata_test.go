package metadata_test

import (
	"annex/metadata"
	"fmt"
	"testing"

	"github.com/go-git/go-git/v5"
)

// 1602066126.61440194s Duration +!MDowMjowOCAoYXBwcm94KQ==
// 1604450323.607679088s Duration +!MDowMjowNyAoYXBwcm94KQ== -!MDowMjowOCAoYXBwcm94KQ==

// Confirm duplicate values are omitted

func TestMain(t *testing.T) {
	repo, err := git.PlainOpen("/home/ben/The_Fall")
	if err != nil {
		panic(err)
	}
	var found int
	met := &metadata.Metadata{Repository: repo}
	if err := met.EachKeyFields(func(kf metadata.KeyFields) error {
		fmt.Printf("%s: %s\n", kf.Key.String(), kf.Fields.String())
		found++
		return nil
	}); err != nil {
		panic(err)
	}
	fmt.Printf("found %d\n", found)
}
