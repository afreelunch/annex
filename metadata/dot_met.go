package metadata

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
)

type dotMet struct {
	Lines []*line
}

func newDotMet() *dotMet {
	return &dotMet{Lines: []*line{}}
}

func (dm *dotMet) Parse(rdr io.Reader) (*Fields, error) {
	if err := dm.parseLines(rdr); err != nil {
		return nil, err
	}
	return dm.parseFields(), nil
}

func (dm *dotMet) parseLines(rdr io.Reader) error {
	dm.Lines = dm.Lines[:0] // fixme: mem leak
	br := bufio.NewReader(rdr)
	for {
		str, err := br.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		ln, err := newLine(str)
		if err != nil {
			return err
		}
		dm.Lines = append(dm.Lines, ln)
	}
	return nil
}

func (dm *dotMet) parseFields() *Fields {
	fields := Fields(make(map[string]*FieldValues))
	for _, ln := range dm.Lines {
		for name, val := range ln.Values {
			if fields[name] == nil {
				fields[name] = &FieldValues{Vs: []string{}}
			}
			fields[name].LastModified = ln.Time
			if val.state == '+' {
				fields[name].Add(val.value)
			}
			if val.state == '-' {
				fields[name].Remove(val.value)
			}
		}
	}
	for n, vs := range fields {
		if len((*vs).Vs) == 0 {
			delete(fields, n)
		}
	}
	return &fields
}

type stateValue struct {
	state byte
	value string
}

type line struct {
	Time   time.Time
	Values map[string]stateValue
}

// todo: ideally we'd only bother with this slightly expensive parsing
// if we knew this time was going to be returned to the caller
func (ln *line) parseTime(str string) error {
	parts := strings.Split(strings.TrimSuffix(str, "s"), ".")
	if len(parts) != 2 {
		return fmt.Errorf("parseTime: malformed field: %s", str)
	}
	secs, err := strconv.ParseInt(parts[0], 10, 64)
	if err != nil {
		return err
	}
	nsecs, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		return err
	}
	ln.Time = time.Unix(secs, nsecs)
	return nil
}

func (ln *line) parseValues(vs []string) error {
	ln.Values = map[string]stateValue{}
	var currentName string
	for i, v := range vs {
		if v == "" {
			return fmt.Errorf("parseValues: empty value")
		}
		if v[0] == '-' || v[0] == '+' {
			if currentName == "" {
				return fmt.Errorf("parseValues: value found before name: %s", v)
			}
			sv, err := newStateValue(v)
			if err != nil {
				return err
			}
			ln.Values[currentName] = sv
		} else {
			currentName = v
			if i+1 == len(vs) {
				return fmt.Errorf("parseValues: found name without value: %s (vs: %v)", currentName, vs)
			}
		}
	}
	return nil
}

func newStateValue(v string) (stateValue, error) {
	if len(v) < 2 {
		return stateValue{}, fmt.Errorf("invalid value: %s", v)
	}
	sv := stateValue{value: v[1:]}
	if v[0] == '+' {
		sv.state = '+'
	} else if v[0] == '-' {
		sv.state = '-'
	} else {
		return sv, fmt.Errorf("value missing +- prefix: %s", v)
	}
	if sv.value[0] == '!' {
		bs, err := base64.StdEncoding.DecodeString(sv.value[1:])
		if err != nil {
			return sv, err
		}
		sv.value = string(bs)
	}
	return sv, nil
}

func newLine(str string) (*line, error) {
	fields := strings.Fields(strings.TrimSuffix(str, "\n"))
	if len(fields) < 3 {
		return nil, fmt.Errorf("too few fields")
	}
	ln := &line{}
	if err := ln.parseTime(fields[0]); err != nil {
		return nil, err
	}
	if err := ln.parseValues(fields[1:]); err != nil {
		return nil, fmt.Errorf("newLine: parseValues: line='%s': %w", str, err)
	}
	return ln, nil
}
