package metadata

import (
	"annex"
	"bytes"
	"fmt"
	"log"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/filemode"
	"github.com/go-git/go-git/v5/plumbing/object"
)

// todo: just parse .git/annex/index?

type Fields map[string]*FieldValues

func (fs *Fields) String() string {
	var bb bytes.Buffer
	for name, fvs := range *fs {
		if fvs == nil {
			continue
		}
		_, _ = bb.WriteString(fmt.Sprintf("%s = %s; ", name, fvs.String()))
	}
	return strings.TrimSuffix(bb.String(), "; ")
}

func (fs *Fields) First(k string) string {
	fvs := (*fs)[k]
	if fvs == nil {
		return ""
	}
	return fvs.First()
}

func NewFields() *Fields {
	f := Fields(make(map[string]*FieldValues))
	return &f
}

type FieldValues struct {
	LastModified time.Time
	Vs           []string
}

func (fv *FieldValues) String() string {
	return strings.Join(fv.Vs, ", ")
}

func (fv *FieldValues) First() string {
	if fv == nil {
		return ""
	}
	if len(fv.Vs) == 0 {
		return ""
	}
	return (*fv).Vs[0]
}

func (fv *FieldValues) Add(val string) {
	for _, existingVal := range (*fv).Vs {
		if existingVal == val {
			return
		}
	}
	(*fv).Vs = append((*fv).Vs, val)
}

func (fv *FieldValues) Remove(val string) {
	var i int
	for _, val2 := range (*fv).Vs {
		if val2 != val {
			(*fv).Vs[i] = val2
			i++
		}
	}
	(*fv).Vs = (*fv).Vs[:i]
}

type KeyFields struct {
	// todo: should either of these be pointers?
	annex.Key
	Fields
}

// todo: name stutters
type Metadata struct {
	*git.Repository
	branch *object.Tree
}

func (met *Metadata) gitAnnexBranch() (*object.Tree, error) {
	if met.branch != nil {
		return met.branch, nil
	}
	ha, err := met.Repository.ResolveRevision(plumbing.Revision("git-annex"))
	if err != nil {
		return nil, fmt.Errorf("gitAnnexBranch: can't resolve rev git-annex: %w", err)
	}
	headCommit, err := met.Repository.CommitObject(*ha)
	if err != nil {
		return nil, fmt.Errorf("gitAnnexBranch: can't get commit object: %w", err)
	}
	branch, err := met.Repository.TreeObject(headCommit.TreeHash)
	if err != nil {
		return nil, fmt.Errorf("gitAnnexBranch: can't get tree object for hash %s: %w", headCommit.TreeHash, err)
	}
	met.branch = branch
	return branch, nil
}

var shardDirPat = regexp.MustCompile(`^[[:xdigit:]]{3}$`)

func (met *Metadata) eachShardTree(root *object.Tree, fn func(*object.Tree) error) error {
	for _, ent := range root.Entries {
		if ent.Mode != filemode.Dir || !shardDirPat.MatchString(filepath.Base(ent.Name)) {
			continue
		}
		subTree, err := met.Repository.TreeObject(ent.Hash)
		if err != nil {
			return err
		}
		if err := met.eachShardTree2(subTree, fn); err != nil {
			return err
		}
	}
	return nil
}

func (met *Metadata) eachShardTree2(root *object.Tree, fn func(*object.Tree) error) error {
	for _, ent := range root.Entries {
		if ent.Mode != filemode.Dir || !shardDirPat.MatchString(filepath.Base(ent.Name)) {
			continue
		}
		subTree, err := met.Repository.TreeObject(ent.Hash)
		if err != nil {
			return err
		}
		if err := fn(subTree); err != nil {
			return err
		}
	}
	return nil
}

func (met *Metadata) eachDotMetInShard(shardTree *object.Tree, fn func(*object.File, *annex.Key) error) error {
	for _, te := range shardTree.Entries {
		if te.Mode != filemode.Regular || !strings.HasSuffix(te.Name, ".log.met") {
			continue
		}
		f, err := shardTree.TreeEntryFile(&te)
		if err != nil {
			return err
		}
		base := filepath.Base(te.Name)
		if strings.HasPrefix(base, "URL-") {
			log.Printf("eachDotMetInShard: Skip URL key: %s (te=%v)", base, te)
			continue
		}
		key, err := annex.NewKey(strings.TrimSuffix(base, ".log.met"))
		if err != nil {
			return fmt.Errorf("eachDotMetInShard: NewKey failed for te=%v, f=%v: %w", te, f, err)
		}
		if err := fn(f, key); err != nil {
			return err
		}
	}
	return nil
}

func (met *Metadata) eachDotMet(root *object.Tree, fn func(*object.File, *annex.Key) error) error {
	return met.eachShardTree(root, func(shardTree *object.Tree) error {
		return met.eachDotMetInShard(shardTree, func(f *object.File, key *annex.Key) error {
			if err := fn(f, key); err != nil {
				return fmt.Errorf("eachDotMet: eachShardTree: eachDotMetInShard: k=%v, err=%v", key, err)
			}
			return nil
		})
	})
}

func (met *Metadata) EachKeyFields(fn func(KeyFields) error) error {
	tree, err := met.gitAnnexBranch()
	if err != nil {
		return fmt.Errorf("met.EachKeyFields: met.gitAnnexBranch: %w", err)
	}
	err = met.eachDotMet(tree, func(f *object.File, k *annex.Key) error {
		rc, err := f.Reader()
		if err != nil {
			return fmt.Errorf("met.eachDotMet (key: %v): f.Reader: %v", k, err)
		}
		defer rc.Close()
		fs, err := newDotMet().Parse(rc)
		if err != nil {
			return fmt.Errorf("newDotMet().Parse: %v", err)
		}
		err = fn(KeyFields{Key: *k, Fields: *fs})
		if err != nil {
			return fmt.Errorf("fn(KeyFields{Key: %v, Fields: %v}: err = %v", *k, *fs, err)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("met.eachDotMet: %v", err)
	}
	return nil
}

func (met *Metadata) ForKey(k *annex.Key) (*Fields, error) {
	tree, err := met.gitAnnexBranch()
	if err != nil {
		return nil, fmt.Errorf("met.gitAnnexBranch: %v", err)
	}
	hd := k.HashDirLower()
	keyPath := filepath.Join(hd, k.String()+".log.met")
	f, err := tree.File(keyPath)
	if err != nil {
		return nil, fmt.Errorf("met.ForKey: no tree file for %s: %w", keyPath, err)
	}
	rc, err := f.Reader()
	if err != nil {
		return nil, fmt.Errorf("met.eachDotMet (key: %v): f.Reader: %v", k, err)
	}
	defer rc.Close()
	fs, err := newDotMet().Parse(rc)
	if err != nil {
		return nil, fmt.Errorf("newDotMet().Parse: %v", err)
	}
	return fs, nil
}

func (met *Metadata) WebForKey(k *annex.Key) (*[]string, error) {
	tree, err := met.gitAnnexBranch()
	if err != nil {
		return nil, fmt.Errorf("met.gitAnnexBranch: %v", err)
	}
	hd := k.HashDirLower()
	keyPath := filepath.Join(hd, k.String()+".log.web")
	f, err := tree.File(keyPath)
	if err != nil {
		return nil, fmt.Errorf("met.WebForKey: no tree file for %s: %w", keyPath, err)
	}
	rc, err := f.Reader()
	if err != nil {
		return nil, fmt.Errorf("met.eachDotMet (key: %v): f.Reader: %v", k, err)
	}
	defer rc.Close()
	webs, err := newDotWeb().Parse(rc)
	if err != nil {
		return nil, fmt.Errorf("newDotMet().Parse: %v", err)
	}
	return webs, nil
}
