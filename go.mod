module gitlab.com/afreelunch/annex

go 1.20

require (
	github.com/go-git/go-git/v5 v5.2.0
	golang.org/x/crypto v0.0.0-20210314154223-e6e6c4f2bb5b // indirect
	golang.org/x/text v0.3.5 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
