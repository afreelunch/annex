package annex

import (
	"fmt"
	"testing"

	"github.com/go-git/go-git/v5"
)

func TestEntries(t *testing.T) {
	repo, err := git.PlainOpen("/home/ben/The_Fall")
	if err != nil {
		panic(err)
	}
	entries := Entries{Repository: repo}
	if err := entries.Each(func(entry *Entry) error {
		if entry.Key == nil {
			return nil
		}
		fmt.Printf("%s created: %s, modified: %s\n", entry.Name, entry.CreatedAt, entry.ModifiedAt)

		return nil
	}); err != nil {
		panic(err)
	}
}
