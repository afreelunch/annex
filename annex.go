package annex

import (
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Key struct {
	backend string
	ha      string
	ext     string
	size    *int64 // nil means unknown
	Key     string
}

func NewKey(str string) (*Key, error) {
	k := &Key{Key: str}
	if err := k.parse(); err != nil {
		return nil, err
	}
	return k, nil
}

func (key *Key) String() string {
	return string((*key).Key)
}

func (key *Key) Size() int64 {
	// fixme: this could lead to bugs
	if key.size == nil {
		return 0
	}
	return *(key.size)
}

func (key *Key) Hash() string { return key.ha }
func (key *Key) Ext() string  { return key.ext }

var gaSymlinkPat = regexp.MustCompile(`^([A-Z0-9]+)(-s([[:digit:]]+))?--([[:xdigit:]]+)(\.[^/]+)?$`)

func (key *Key) parse() error {
	matches := gaSymlinkPat.FindStringSubmatch(string((*key).Key))
	if matches == nil {
		return fmt.Errorf("key.parse (key=%v): symlink regexp didn't match", string((*key).Key))
	}
	key.backend = matches[1]
	key.ha = matches[4]
	key.ext = matches[5]
	if key.ext != "" && !strings.HasSuffix(key.backend, "E") {
		return fmt.Errorf("backend doesn't support extensions but key has one")
	}
	// todo: verify valid backend (although if new ones are added...)
	// todo: verify hash length vs. backend
	if matches[3] != "" {
		i, err := strconv.ParseInt(matches[3], 10, 64)
		if err != nil {
			return err
		}
		key.size = &i
	}
	return nil
}

func (key *Key) HashDirLower() string {
	ha := md5.New()
	io.WriteString(ha, key.String())
	haHex := fmt.Sprintf("%x", ha.Sum(nil))
	return fmt.Sprintf("%s/%s", haHex[:3], haHex[3:6])
}

const (
	annexKeyChars = "0123456789zqjxkmvwgpfZQJXKMVWGPF"
)

// todo: ensure we only MD5 once per key
func (key *Key) HashDirMixed() string {
	var l [4]string
	ha := md5.New()
	io.WriteString(ha, key.String())
	m := ha.Sum(nil)
	w := binary.LittleEndian.Uint32(m[:4])
	for i := uint(0); i < 4; i++ {
		l[i] = string(annexKeyChars[int(w>>(6*i)&0x1F)])
	}
	return l[1] + l[0] + string(os.PathSeparator) +
		l[3] + l[2] + string(os.PathSeparator)
}

func (k *Key) Scan(src any) error {
	if src == nil {
		return nil
	}
	srcStr, ok := src.(string)
	if !ok {
		return fmt.Errorf("Key.Scan(%T): can't scan type", src)
	}
	k.Key = srcStr
	return k.parse()
}
